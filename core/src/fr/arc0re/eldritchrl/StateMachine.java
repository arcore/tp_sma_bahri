package fr.arc0re.eldritchrl;

public class StateMachine {
    private State currentState;

    public enum State {
        MAIN_MENU,
        MAIN_MENU_SETTINGS,
        ENTERING_GAME,
        IN_GAME,
        PAUSED,
        QUITTING,
        LOADING_LEVEL
    }

    public StateMachine(State startingState) {
        currentState = startingState;
    }

    public State getCurrentState() {
        return currentState;
    }

    public void setCurrentState(State currentState) {
        this.currentState = currentState;
    }
}
