package fr.arc0re.eldritchrl;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;

import java.awt.*;

public class UI extends Actor {
    private Rectangle popupRectangle;
    private ShapeRenderer shapeRenderer;
    private static int popupWidth = Gdx.graphics.getWidth()/2;
    private static int popupHeight = 200;
    private int gapFromTop = 50;
    private String popupText;
    public static BitmapFont font = new BitmapFont();
    private boolean drawPopup = false;
    private float popupOpacity = 1f; //0.3f;;

    public String cStr, eStr, dStr;
    private static GlyphLayout layout = new GlyphLayout();

    // TODO: singleton
    public UI() {
        popupRectangle = new Rectangle(Gdx.graphics.getWidth() / 2 - (popupWidth / 2), (Gdx.graphics.getHeight() - gapFromTop) - popupHeight, popupWidth, popupHeight);
        shapeRenderer = new ShapeRenderer();
        popupText = "popupText";
        eStr = "Use";//EldritchRL.gameStrings.getTraductionFor(Defines.Strings.INVENTORY_POPUP_USE);
        cStr = EldritchRL.gameStrings.getTraductionFor(Defines.Strings.INVENTORY_POPUP_CANCEL);
        dStr = "Discard";
        layout.setText(font, String.format("C: %s", cStr)); // TODO: Avoid repetition with third call to font.draw (line 58)
        layout.setText(font, String.format("D: %s", dStr));
    }

    @Override
    public void draw(Batch batch, float alpha) {
        if (!drawPopup) return;

        batch.end();

        Gdx.gl.glEnable(GL20.GL_BLEND);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);

        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        //shapeRenderer.setColor(new Color(255, 0, 255, popupOpacity));
        shapeRenderer.setColor(new Color(0, 0, 0, popupOpacity));
        shapeRenderer.rect(popupRectangle.x, popupRectangle.y, popupRectangle.width, popupRectangle.height);
        shapeRenderer.end();

        Gdx.gl.glDisable(GL20.GL_BLEND);

        batch.begin();
        font.draw(batch, popupText, popupRectangle.x + 10, popupRectangle.y + (popupRectangle.height - 10));
        font.draw(batch, String.format("E: %s", eStr), (popupRectangle.width/2)+10, popupRectangle.y + 20);
        font.draw(batch, String.format("C: %s", cStr), (popupRectangle.width/2)+layout.width+10, popupRectangle.y + 20);
        font.draw(batch, String.format("D: %s", dStr), (popupRectangle.width/2)+layout.width+100, popupRectangle.y + 20);
    }

    public void showPopup(String text) {
        popupText = text;
        drawPopup = true;
    }

    public void killPopup() {
        drawPopup = false;
        popupText = "";
    }

    public boolean popupIsAlive() {
        return drawPopup;
    }
}
