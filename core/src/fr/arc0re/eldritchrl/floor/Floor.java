package fr.arc0re.eldritchrl.floor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ai.pfa.indexed.IndexedAStarPathFinder;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import fr.arc0re.eldritchrl.Tile;
import fr.arc0re.eldritchrl.ai.Graph;
import fr.arc0re.eldritchrl.ai.GraphGenerator;
import fr.arc0re.eldritchrl.ai.Node;
import fr.arc0re.eldritchrl.entity.Entity;
import fr.arc0re.eldritchrl.entity.EntityMonster;
import fr.arc0re.eldritchrl.entity.Player;
import fr.arc0re.eldritchrl.util.Util;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public abstract class Floor extends Actor {
    public String name;
    public Map<Integer, Tile> tileHash;
    /** List of every Tile object of the integer tilemap that makes the Floor */
    public ArrayList<Tile> tileList;
    public Class<? extends Floor> nextFloorClass;
    public int[][] cells; // pregen
    public int[][] newCells; // postgen
    public int width;
    public int height;
    public static TextureAtlas tilesTexture;
    protected TextureRegion greenTile;
    protected TextureRegion blueTile;
    public static final String TILES_TEXTURE_ATLAS_PACK_FILE = "tiles/packed/tiles.atlas";
    public static final int TILE_WIDTH = 16;
    public static final int TILE_HEIGHT = 16;
    public static final float TILE_SCALE = 4.0f;
    public final int mapCols = 48;
    public final int mapRows = 48;
    protected static Random random = new Random();
    public ArrayList<Entity> entities;
    public Tile exitTile;

    // AI
    public IndexedAStarPathFinder<Node> pathfinder;
    public Graph graph;

    protected Player playerRef;

    public class Tiles {
        public static final int GROUND_01 = 0;
        public static final int GROUND_02 = 1;
        public static final int GROUND_03 = 2;
    }

    public Floor() {
        cells = new int[mapCols][mapRows];
        newCells = new int[mapCols][mapRows];
        entities = new ArrayList<Entity>();

        //TODO: fuck this ?
        this.width = cells.length;
        this.height = cells[0].length;

        tilesTexture = new TextureAtlas(Gdx.files.internal(TILES_TEXTURE_ATLAS_PACK_FILE));
        generateDungeon();
        setupTiles();
        placeMonsters();
        placeFloorExit();

        graph = GraphGenerator.generateGraph(newCells, mapRows, mapCols);
        pathfinder = new IndexedAStarPathFinder<Node>(graph);
    }

    /** Called by the constructor */
    public void setupTiles() {
        greenTile = tilesTexture.findRegion("green");
        blueTile = tilesTexture.findRegion("blue");

        tileHash = new HashMap<Integer, Tile>();
        tileHash.put(0, new Tile(true, false, "GROUND_01", new Point(0, 0), 0, tilesTexture.findRegion("green")));
        tileHash.put(1, new Tile(true, false, "GROUND_02", new Point(0, 0), 1, tilesTexture.findRegion("blue")));

        tileList = new ArrayList<Tile>();
        for (int y = 0; y < mapCols; y++) {
            for (int x = 0; x < mapRows; x++) {
                int tileInt = cells[x][y];
                if (tileHash.containsKey(tileInt)) {
                    Tile tile = tileHash.get(tileInt);
                    tile.coords = new Point(x, y);
                    tileList.add(new Tile(tile));
                }
            }
        }
    }

    /** Called by the constructor */
    public abstract void generateDungeon();

    /** Called by the constructor */
    public abstract void placeItems();

    /** Called by the constructor */
    public abstract void placeMonsters();

    /** get Tile at screen coords */
    public Tile getTileAtGameCoords(int x, int y) {
        int xx = x / Floor.TILE_WIDTH;
        int yy = y / Floor.TILE_HEIGHT;
        Point p = new Point(xx, yy);

        if (tileList == null) return null;

        for (Tile tile : tileList) {
            if (tile.coords.equals(p)) {
                return tile;
            }
        }
        return null;
    }

    public Tile getTileAtGameCoords(Point screenCoords) {
        return getTileAtGameCoords(screenCoords.x, screenCoords.y);
    }

    public Tile getTileAt(int x, int y) {
        Point p = getScreenCoordsOfTile(x, y);
        return getTileAtGameCoords(p);
    }

    public Point getScreenCoordsOfTile(int x, int y) {
        int xx = x * Floor.TILE_WIDTH;
        int yy = y * Floor.TILE_HEIGHT;
        return new Point(xx, yy);
    }

    public Point getScreenCoordsOfTile(Point tileCoords) {
        return getScreenCoordsOfTile(tileCoords.x, tileCoords.y);
    }

    public Point getScreenCoordsOfTile(Tile theTile) {
        return getScreenCoordsOfTile(theTile.coords.x, theTile.coords.y);
    }

    protected void drawTile(Tile tile, int x, int y, Batch batch) {
        Sprite sprite;
        if (!tile.discovered) {
            sprite = new Sprite(tilesTexture.findRegion("unseen"));
        } else if (tile.isFloorExit) {
            sprite = new Sprite(tilesTexture.findRegion("floor_exit"));
        } else {
            sprite = new Sprite(tile.textureRegion);
        }
        sprite.setPosition(x * TILE_WIDTH, y * TILE_HEIGHT);
        sprite.flip(false, true);
        sprite.draw(batch);

        drawTileTopItem(tile, x, y, batch);
        drawTileEntity(tile, batch);
    }

    protected void drawTileTopItem(Tile tile, int x, int y, Batch batch) {
        if (!tile.discovered) return;
        if (tile.peekTopItem() == null) return;

        Sprite sprite = new Sprite(tilesTexture.findRegion(tile.peekTopItem().textureRegionName));
        sprite.setPosition(x * TILE_WIDTH, y * TILE_HEIGHT);
        sprite.flip(false, true);
        sprite.draw(batch);
    }

    protected void drawTileEntity(Tile tile, Batch batch) {
        if (!tile.discovered) return;
        if (!tile.containsEntity) return;
        if (!tile.getCurrentEntity().alive) {
            tile.removeEntity(); // TODO: Put that elsewhere?????
            return;
        }

        tile.getCurrentEntity().draw(batch, 0);
    }

    @Override
    public void draw(Batch batch, float alpha) {
        for (int y = 0; y < mapCols; y++) {
            for (int x = 0; x < mapRows; x++) {
                drawTile(getTileAt(x, y), x, y, batch);
            }
        }
    }

    public void revealAllTiles() {
        for (Tile tile : tileList) {
            tile.discovered = true;
        }
    }

    public Player getPlayerRef() {
        return playerRef;
    }

    public void setPlayerRef(Player playerRef) {
        this.playerRef = playerRef;
    }

    /** Called by the constructor. */
    protected void placeFloorExit() {
        boolean exitPlaced = false;
        do {
            int xExit = Util.getRandomInt(0, mapRows);
            int yExit = Util.getRandomInt(0, mapCols);
            try {
                if (newCells[xExit][yExit] != 1 && getTileAt(xExit, yExit) != null) {// Not wall
                    getTileAt(xExit, yExit).setAsExit();
                    exitPlaced = true;
                    this.exitTile = getTileAt(xExit, yExit);
                    System.out.printf("Exit placed at tile %d,%d", getTileAt(xExit, yExit).coords.x, getTileAt(xExit, yExit).coords.y);
                }
            } catch (ArrayIndexOutOfBoundsException ex) {
                System.out.println("Caught OutOfBounds in placeFloorExit... Continuing");
            }
        } while (!exitPlaced);
    }

    protected void spawn(Class<? extends EntityMonster> monster, Tile tile) {
        try {
            EntityMonster newmon = monster.getDeclaredConstructor(Floor.class).newInstance(this);
            newmon.screenCoords = getScreenCoordsOfTile(tile);
            tile.addEntity(newmon); entities.add(newmon);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
