package fr.arc0re.eldritchrl.floor;

import fr.arc0re.eldritchrl.Tile;
import fr.arc0re.eldritchrl.entity.EntityRabbit;
import fr.arc0re.eldritchrl.entity.EntityFox;
import fr.arc0re.eldritchrl.util.Util;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;

public class FloorSimu extends Floor {

    @Override
    public void generateDungeon() {

    }

    @Override
    public void placeItems() {

    }

    @Override
    public void setupTiles() {
        tileHash = new HashMap<Integer, Tile>();
        tileHash.put(0, new Tile(true, false, "floor covered in crunchy moss", new Point(0, 0), 0, tilesTexture.findRegion("entrance_floor")));
        tileHash.put(1, new Tile(false, false, "wall covered in moss", new Point(0, 0), 1, tilesTexture.findRegion("entrance_wall")));
        tileHash.put(2, new Tile(true, false, "blue potion", new Point(0, 0), 2, tilesTexture.findRegion("blue_potion")));

        tileList = new ArrayList<Tile>();
        for (int y = 0; y < mapCols; y++) {
            for (int x = 0; x < mapRows; x++) {
                int tileInt = newCells[x][y];
                if (tileHash.containsKey(tileInt)) {
                    Tile tile = tileHash.get(tileInt);
                    tile.coords = new Point(x, y);
                    if (tileInt == 0) {
                        String region = (random.nextInt() % 2 == 0) ? "entrance_floor" : "entrance_floor_grass_two";
                        tile.textureRegion = tilesTexture.findRegion(region);
                    }
                    tileList.add(new Tile(tile));
                }
            }
        }
    }

    @Override
    public void placeMonsters() {
        int maxRabbits = 20;
        int numOfRabbits = maxRabbits;
        int maxFoxes = 10;

        for (int x = mapRows/2; x > 0; x--) {
            for (int y = mapCols/2; y > 0 ; y--) {
                if (numOfRabbits == maxRabbits/2) break;
                Tile tile = getTileAt(x, y);
                if (tile!=null && tile.isWalkable && !tile.containsEntity && tile.items.size() == 0) {
                    if (Util.getRandomInt(0, 100) < 5) { // 5% chance of spawning a rat
                        numOfRabbits--;
                        spawn(EntityRabbit.class, tile);
                    }
                }
            }
        }
        for (int x = 0; x < mapRows; x++) {
            for (int y = 0; y < mapCols ; y++) {
                if (numOfRabbits == 0) break;
                Tile tile = getTileAt(x, y);
                if (tile!=null && tile.isWalkable && !tile.containsEntity && tile.items.size() == 0) {
                    if (Util.getRandomInt(0, 100) < 5) { // 5% chance of spawning a rat
                        numOfRabbits--;
                        spawn(EntityRabbit.class, tile);
                    }
                }
            }
        }

        for (int x=mapRows;x>0;x--) for (int y=mapCols;y>0;y--){
            if (maxFoxes == 0) break;
            Tile tile = getTileAt(x,y);
            if (tile!=null && tile.isWalkable && !tile.containsEntity && tile.items.size() == 0) {
                if (Util.getRandomInt(0, 100) < 10) { // 10% chance to spawn
                    spawn(EntityFox.class, tile);
                    maxFoxes--;
                }
            }
        }
    }
}
