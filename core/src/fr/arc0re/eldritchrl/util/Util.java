package fr.arc0re.eldritchrl.util;

import java.util.concurrent.ThreadLocalRandom;

public final class Util {

    public static int getRandomInt(int min, int max) {
        return ThreadLocalRandom.current().nextInt(min, max + 1);
    }

    private Util() {
    }
}
