package fr.arc0re.eldritchrl.util;

public class Tuple<T1, T2> {
    public final T1 firstElement;
    public final T2 secondElement;

    public Tuple(T1 firstElement, T2 secondElement) {
        this.firstElement = firstElement;
        this.secondElement = secondElement;
    }
}
