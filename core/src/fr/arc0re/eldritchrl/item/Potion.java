package fr.arc0re.eldritchrl.item;

public abstract class Potion extends Item {
    /** Duration of the potion effect in turns */
    public int duration = 5;

    public Potion() {
    }
}
