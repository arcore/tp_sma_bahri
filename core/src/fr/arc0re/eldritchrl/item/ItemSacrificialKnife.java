package fr.arc0re.eldritchrl.item;

public class ItemSacrificialKnife extends ItemWeapon {
    public ItemSacrificialKnife() {
        this.attackBoost = 15;
    }

    @Override
    public void setupItem() {
        this.textureRegionName = "city_sacrificial_knife";
        this.name = "Sacrificial knife";
        this.description = "A weird tainted blade used to... kill... people?";
    }
}
