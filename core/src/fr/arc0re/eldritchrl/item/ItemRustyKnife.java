package fr.arc0re.eldritchrl.item;

public class ItemRustyKnife extends ItemWeapon {
    public ItemRustyKnife() {
        this.attackBoost = 10;
    }

    @Override
    public void setupItem() {
        this.textureRegionName = "entrance_rusty_knife";
        this.name = "Rusty knife";
        this.description = "An abandonned knife that seems ready to break.";
    }
}
