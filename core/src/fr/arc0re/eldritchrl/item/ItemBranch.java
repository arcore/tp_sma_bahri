package fr.arc0re.eldritchrl.item;

public class ItemBranch extends ItemWeapon {
    public ItemBranch() {
        this.attackBoost = 5;
    }

    @Override
    public void setupItem() {
        this.textureRegionName = "entrance_weapon_wood";
        this.name = "Wood branch";
        this.description = "A simple fragile wood branch, fallen from some tree.";
    }
}
