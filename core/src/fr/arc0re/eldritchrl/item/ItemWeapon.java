package fr.arc0re.eldritchrl.item;

import fr.arc0re.eldritchrl.EldritchRL;
import fr.arc0re.eldritchrl.entity.Entity;
import fr.arc0re.eldritchrl.entity.Player;

public abstract class ItemWeapon extends Item {
    public int attackBoost;

    public ItemWeapon() {
    }

    @Override
    public void use(Entity entity) {
        Player player = (Player)entity;

        // We put the old weapon back in the inventory.
        if (player.inventory.equippedWeapon != null)
            player.inventory.items.add(player.inventory.equippedWeapon);

        player.inventory.equippedWeapon = this;

        EldritchRL.infoTextBuffer = String.format("%s equipped %s!\n", player.name, this.name);

        player.resetPlayerStrength();
        player.attack += attackBoost;
    }
}
