package fr.arc0re.eldritchrl.item;

import com.badlogic.gdx.scenes.scene2d.Actor;
import fr.arc0re.eldritchrl.entity.Entity;

public abstract class Item extends Actor {
    public String textureRegionName;
    public String name;
    public String description;

    public Item() {
        setupItem();
    }

    public void setupItem() {
    }

    public abstract void use(Entity entity);
}
