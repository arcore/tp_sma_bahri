package fr.arc0re.eldritchrl.item;

import fr.arc0re.eldritchrl.Defines;
import fr.arc0re.eldritchrl.EldritchRL;
import fr.arc0re.eldritchrl.entity.Entity;

public class BluePotion extends Potion {

    public BluePotion() {
    }

    @Override
    public void setupItem() {
        this.textureRegionName = "blue_potion";
        this.name = EldritchRL.gameStrings.getTraductionFor(Defines.Strings.BLUE_POTION_NAME);
        this.description = EldritchRL.gameStrings.getTraductionFor(Defines.Strings.BLUE_POTION_DESCRIPTION);
    }

    @Override
    public void use(Entity entity) {
        int actualBonus = 20;

        if (entity.health == entity.maxHealth) {
            EldritchRL.infoTextBuffer = "Mmm... looks like you wasted this... (+0 health)";
            return;
        }

        if (entity.health + actualBonus > entity.maxHealth) {
            actualBonus = entity.maxHealth-entity.health;
        }
        entity.health += actualBonus;
        EldritchRL.infoTextBuffer = String.format("Ahh, you feel better! (+%d health)", actualBonus);
    }
}
