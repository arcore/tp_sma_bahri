package fr.arc0re.eldritchrl.item;

public class ItemDiamondSword extends ItemWeapon {
    public ItemDiamondSword() {
        this.attackBoost = 10;
    }

    @Override
    public void setupItem() {
        this.textureRegionName = "diamond_sword";
        this.name = "Diamond sword";
        this.description = "A strange sword made in a pure rock.";
    }
}
