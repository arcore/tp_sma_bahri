package fr.arc0re.eldritchrl;

import com.badlogic.gdx.Input;

public class Defines {
    public static final int DESKTOP_WINDOW_WIDTH = 800;
    public static final int DESKTOP_WINDOW_HEIGHT = 600;
    public static final String DESKTOP_WINDOW_TITLE = "EldritchRL";
    public static final boolean USE_DEBUG_MODE = true;

    // TODO: config file, arg
    public static final String GAME_LOCALE = "EN";

    /** Contains all game strings used for localisation. */
    public static class Strings {
        public static final String MAIN_MENU_NEW_GAME = "New Game";
        public static final String MAIN_MENU_SETTINGS = "Settings";
        public static final String MAIN_MENU_QUIT = "Exit to OS";

        public static final String INVENTORY_INVENTORY = "Inventory";
        public static final String INVENTORY_POPUP_USE = "Use";
        public static final String INVENTORY_POPUP_CANCEL = "Cancel";

        public static final String INFO_POPUP_USED_ITEM = "Used item ";

        public static final String BLUE_POTION_NAME = "Blue potion";
        public static final String BLUE_POTION_DESCRIPTION = "Strange blue colored potion with tiny bubbles\nthat has a very strong sugar taste.\nSeems to make people excited...\n";
    }

    public static class PlayerBindings {
        public static final int UP = Input.Keys.UP; //Input.Keys.K;
        public static final int DOWN = Input.Keys.DOWN; //Input.Keys.J;
        public static final int LEFT = Input.Keys.LEFT;//Input.Keys.H;
        public static final int RIGHT = Input.Keys.RIGHT;//Input.Keys.L;
        public static final int INV = Input.Keys.I;
    }

    private Defines() {}
}
