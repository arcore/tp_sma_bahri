package fr.arc0re.eldritchrl.locale;

import fr.arc0re.eldritchrl.Defines;

public class FrStrings implements IStrings {

    public FrStrings() {
        String[][] pairs = {
            { Defines.Strings.MAIN_MENU_NEW_GAME, "Nouvelle Partie" },
            { Defines.Strings.MAIN_MENU_SETTINGS, "Options" },
            { Defines.Strings.MAIN_MENU_QUIT, "Quitter" },
            { Defines.Strings.INVENTORY_INVENTORY, "Inventaire" },
            { Defines.Strings.INVENTORY_POPUP_CANCEL, "Annuler" },
            { Defines.Strings.INVENTORY_POPUP_USE, "Utiliser" },
            { Defines.Strings.INFO_POPUP_USED_ITEM, "Utilisé " },
            { Defines.Strings.BLUE_POTION_NAME, "Potion bleue" },
            { Defines.Strings.BLUE_POTION_DESCRIPTION, "Etrange potion de couleur bleue pétillante\ntrès sucrée.\nSemble rendre les gens nerveux...\n" }
        };
        for (String[] pair : pairs) {
             stringsHashMap.put(pair[0], pair[1]);
        }
    }

    @Override
    public String getTraductionFor(String constGameString) {
        String str = stringsHashMap.get(constGameString);
        if (str != null) return str;
        else return constGameString;
    }
}
