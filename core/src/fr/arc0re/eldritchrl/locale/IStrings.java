package fr.arc0re.eldritchrl.locale;

import java.util.HashMap;
import java.util.Map;

public interface IStrings {
    public Map<String, String> stringsHashMap = new HashMap<String, String>();

    public String getTraductionFor(String constGameString);
}
