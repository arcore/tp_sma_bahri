package fr.arc0re.eldritchrl;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.ai.pfa.DefaultGraphPath;
import com.badlogic.gdx.ai.pfa.GraphPath;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import fr.arc0re.eldritchrl.ai.ManhattanDistance;
import fr.arc0re.eldritchrl.ai.Node;
import fr.arc0re.eldritchrl.entity.Entity;
import fr.arc0re.eldritchrl.entity.EnumPlayerState;
import fr.arc0re.eldritchrl.entity.Player;
import fr.arc0re.eldritchrl.floor.Floor;
import fr.arc0re.eldritchrl.floor.FloorSimu;
import fr.arc0re.eldritchrl.locale.EnStrings;
import fr.arc0re.eldritchrl.locale.FrStrings;
import fr.arc0re.eldritchrl.locale.IStrings;
import fr.arc0re.eldritchrl.util.Tuple;

public class EldritchRL extends ApplicationAdapter {
    public static final float CAMERA_ZOOM = 0.02f;
    public static final int CAMERA_MOVE_SPEED = 4;
    public StateMachine mainStateMachine;
	private SpriteBatch batch;
	private SpriteBatch HUDBatch;
	private BitmapFont bitmapFont;
    public static IStrings gameStrings;
    private Stage mainMenuStage;
    private TextButton newGameTextBtn;
    private TextButton settingsTextBtn;
    private TextButton quitToOSbtn;
    private OrthographicCamera camera;
    private Stage gameStage;
    private Floor currentFloor;
    private String debugInfo;
    public static String inventoryTextBuffer;
    public static String infoTextBuffer;
    public static String playerStatsTextBuffer;
    private Player player;
    private static GlyphLayout layout = new GlyphLayout();
    private UI gameUI;
    private boolean playerJustDied = false;
    private boolean reloadingCurrentLevel = false;

    public UI getGameUI() {
        return gameUI;
    }

    // INIT, ASSET LOADING, ETC
	@Override
	public void create() {
	    mainStateMachine = new StateMachine(StateMachine.State.MAIN_MENU);
		batch = new SpriteBatch();
		HUDBatch = new SpriteBatch();
        bitmapFont = new BitmapFont();
        camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.setToOrtho(true, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.position.set(camera.viewportWidth / 2f, camera.viewportHeight / 2f, 0);
        camera.zoom = 0.79f;//0.36f;//0.22f;
        debugInfo = "debugInfo";
        inventoryTextBuffer = "";
        infoTextBuffer = "";
        playerStatsTextBuffer = "";

        if (Defines.GAME_LOCALE.equals("FR")) {
            gameStrings = new FrStrings();
        } else {
            gameStrings = new EnStrings();
        }

        currentFloor = new FloorSimu();
        currentFloor.revealAllTiles();
        player = new Player(currentFloor);
        gameUI = new UI();

        currentFloor.setPlayerRef(player);

        player.setGameInstance(this);

        String newGameText = gameStrings.getTraductionFor(Defines.Strings.MAIN_MENU_NEW_GAME);
        String settingsText = gameStrings.getTraductionFor(Defines.Strings.MAIN_MENU_SETTINGS);
        String quitText = gameStrings.getTraductionFor(Defines.Strings.MAIN_MENU_QUIT);

        Table mainMenuContainer = new Table();
        mainMenuStage = new Stage();

        // Main menu buttons
        // TODO: move all main menu stuff to a class that inherits from Stage?
        {
            TextButton.TextButtonStyle tbs = new TextButton.TextButtonStyle();
            tbs.font = bitmapFont;

            Label.LabelStyle lbs = new Label.LabelStyle();
            lbs.font = bitmapFont;
            lbs.fontColor = Color.TEAL;
            Label titleLbl = new Label("EldritchRL", lbs);
            mainMenuContainer.add(titleLbl).expandX().center().row();

            newGameTextBtn = new TextButton(newGameText, tbs);
            newGameTextBtn.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    if (mainStateMachine.getCurrentState() != StateMachine.State.IN_GAME) {
                        mainStateMachine.setCurrentState(StateMachine.State.ENTERING_GAME);
                        if (playerJustDied) {
                            currentFloor = new FloorSimu();
                            player = new Player(currentFloor);
                            currentFloor.setPlayerRef(player);
                        }
                        System.out.println("CHANGED STATE TO ENTERING_GAME!");
                    }
                }
            });
            mainMenuContainer.add(newGameTextBtn).expandX().center().row();

            /*settingsTextBtn = new TextButton(settingsText, tbs);
            settingsTextBtn.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    mainStateMachine.setCurrentState(StateMachine.State.MAIN_MENU_SETTINGS); //TODO
                    System.out.println("CHANGED STATE TO SETTINGS!");
                }
            });
            mainMenuContainer.add(settingsTextBtn).expandX().center().row();*/

            quitToOSbtn = new TextButton(quitText, tbs);
            quitToOSbtn.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    if (mainStateMachine.getCurrentState() != StateMachine.State.IN_GAME) {
                        mainStateMachine.setCurrentState(StateMachine.State.QUITTING); //TODO
                        System.out.println("CHANGED STATE TO QUIT !");
                    }
                }
            });
            mainMenuContainer.add(quitToOSbtn).expandX().center().row();
        }
        mainMenuContainer.setFillParent(true);
        mainMenuStage.addActor(mainMenuContainer);
        Gdx.input.setInputProcessor(mainMenuStage);
    }

    private void handleInput() {
	    if (mainStateMachine.getCurrentState() == StateMachine.State.IN_GAME) {
            if (Gdx.input.isKeyPressed(Input.Keys.P)) {
                camera.zoom += CAMERA_ZOOM;
            }
            if (Gdx.input.isKeyPressed(Input.Keys.M)) {
                camera.zoom -= CAMERA_ZOOM;
            }
            if (Gdx.input.isKeyPressed(Input.Keys.R)) {
                System.out.println("%%%%%%%% REVEALING ALL TILES %%%%%%%%");
                currentFloor.revealAllTiles();
            }
        }

        if (mainStateMachine.getCurrentState() == StateMachine.State.MAIN_MENU) {
            if (Gdx.input.isKeyPressed(Input.Keys.N)) {
                mainStateMachine.setCurrentState(StateMachine.State.ENTERING_GAME);
            }
        }

        if (Defines.USE_DEBUG_MODE) {
            if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
                System.out.println("cam moving left");
                camera.translate(-CAMERA_MOVE_SPEED, 0, 0);
            }
            if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
                System.out.println("cam moving right");

                camera.translate(CAMERA_MOVE_SPEED, 0, 0);
            }
            if (Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
                System.out.println("cam moving down");

                camera.translate(0, CAMERA_MOVE_SPEED, 0);
            }
            if (Gdx.input.isKeyPressed(Input.Keys.UP)) {
                System.out.println("cam moving up");

                camera.translate(0, -CAMERA_MOVE_SPEED, 0);
            }
        }

        if (Gdx.input.isKeyPressed(Input.Keys.Q)) {
	        //mainStateMachine.setCurrentState(StateMachine.State.QUITTING);
        }
    }

	// MAIN LOOP
	@Override
	public void render() {
        long endPauseTime = System.currentTimeMillis() + (10*1000);
        if (endPauseTime < System.currentTimeMillis()) {
            handleInput();
        } else {
            handleInput();


            // Follow player
            if (player != null && mainStateMachine.getCurrentState() == StateMachine.State.IN_GAME) {
                //camera.position.set(new Vector2(player.screenCoords.x+Floor.TILE_WIDTH/2, player.screenCoords.y+Floor.TILE_HEIGHT/2), 0);
            }
            camera.update();

            float delta = Gdx.graphics.getDeltaTime();

            //Gdx.gl.glClearColor(0/255.0f, 28/255.0f, 27/255.0f, 0.0f);
            Gdx.gl.glClearColor(0, 0, 0, 0);
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

            batch.setProjectionMatrix(camera.combined);
            batch.begin();

            // State checking
            if (mainStateMachine != null) {
                switch (mainStateMachine.getCurrentState()) {
                    case MAIN_MENU: {
                        mainMenuStage.draw();
                    }
                    break;

                    case ENTERING_GAME: {
                        // Check if the player can spawn and move
                        if (!player.getCurrentTile().isWalkable) {
                            for (Tile tile : currentFloor.tileList) {
                                player.screenCoords.x += 1;
                                player.screenCoords.y += 1;
                                if (player.getCurrentTile().isWalkable) {
                                    if (player.getCurrentTile().items.size() > 0)
                                        player.getCurrentTile().items.clear(); // TODO: no
                                    break;
                                }
                            }
                        }

                    /*if (!playerCanReachExit()) {
                        reloadingCurrentLevel = true;
                        mainStateMachine.setCurrentState(StateMachine.State.LOADING_LEVEL);
                        break;
                    }*/

                        mainStateMachine.setCurrentState(StateMachine.State.IN_GAME);
                        System.out.println("NOW IN_GAME !");
                    }
                    break;

                    case LOADING_LEVEL: {
                        try {
                            // Switching to next level
                            if (currentFloor.nextFloorClass != null /*&& !reloadingCurrentLevel*/) {
                                currentFloor = currentFloor.nextFloorClass.newInstance();
                            } else {
                                mainStateMachine.setCurrentState(StateMachine.State.MAIN_MENU);
                                break;
                            }
                        /* else {
                            // Reloading current level because of generation failure:
                            Class<? extends Floor> floorType = currentFloor.getClass();
                            currentFloor = floorType.newInstance();
                            reloadingCurrentLevel = false;
                        }*/

                            player.screenCoords.x = 1;
                            player.screenCoords.y = 1;
                            currentFloor.setPlayerRef(player);
                            player.setCurrentFloor(currentFloor);

                            for (Entity ent : currentFloor.entities) ent.setCurrentFloor(currentFloor);
                            for (Tile t : player.getEightAdjacentTiles()) if (t != null) t.discovered = true;

                            mainStateMachine.setCurrentState(StateMachine.State.ENTERING_GAME);
                        } catch (Exception e) {
                            e.printStackTrace();
                            mainStateMachine.setCurrentState(StateMachine.State.QUITTING);
                        }
                    }
                    break;

                    case IN_GAME: {
                        if (player.state == EnumPlayerState.DEAD) {
                            playerJustDied = true;
                            mainStateMachine.setCurrentState(StateMachine.State.MAIN_MENU);
                        }

                        if (player.getCurrentTile().isFloorExit) {
                            mainStateMachine.setCurrentState(StateMachine.State.LOADING_LEVEL);
                        }

                        Vector3 coords = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);
                        camera.unproject(coords);
                        Tile tile = currentFloor.getTileAtGameCoords((int) coords.x, (int) coords.y);
                        if (tile != null && tile.discovered) {
                            if (tile.containsEntity)
                                debugInfo = String.format("You see a %s.",
                                        ((tile.getCurrentEntity().description != null && tile.getCurrentEntity().description.length() > 0)
                                                ? tile.getCurrentEntity().description
                                                : tile.getCurrentEntity().name));
                            else if (tile.items.size() > 0)
                                debugInfo = String.format("You see a %s.", tile.items.peek().name);
                            else
                                debugInfo = String.format("You see a %s.", tile.name);
                        } else if (tile != null) {
                            debugInfo = "You can't see through the shadows...";
                        } else {
                            debugInfo = "Nothing here.";
                        }

                        playerStatsTextBuffer = String.format("Health: %d [MaxHealth: %d]\nAttack: %d\nEquipped ItemWeapon: %s\nCurrent level: %d / XP before next level: %d",
                                player.health, player.maxHealth, player.attack,
                                ((player.inventory.equippedWeapon != null)
                                        ? player.inventory.equippedWeapon.name
                                        : "None"),
                                player.level, player.xpBeforeLvlUp);
                        playerStatsTextBuffer += ((player.godmode) ? "\n-- GODMODE ENABLED --" : "");
                        currentFloor.draw(batch, 0);
                        player.draw(batch, 0);

                        // AUTO STEP
                        step();
//                        new Thread(new Runnable() {
//                            @Override
//                            public void run() {
//                                Gdx.app.postRunnable(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        float delay = 2; // seconds
//                                        Timer.schedule(new Timer.Task(){
//                                            @Override
//                                            public void run() {
//                                                step();
//                                            }
//                                        }, delay);
//                                    }
//                                });
//                            }
//                        }).start();

                    }
                    break;

                    case PAUSED: {
                    }
                    break;

                    case QUITTING: {
                        System.out.println("Quitting game!");
                        Gdx.app.exit();
                    }
                    break;
                }
            } else {
                Gdx.app.log("ERROR", "mainStateMachine is null! Abort!");
            }

            batch.end();

            if (mainStateMachine.getCurrentState() == StateMachine.State.IN_GAME) {
                HUDBatch.begin();
                bitmapFont.setColor(com.badlogic.gdx.graphics.Color.WHITE);
                bitmapFont.draw(HUDBatch, debugInfo, 10, 20);
//            bitmapFont.draw(HUDBatch, playerDebugInfo, 10, Gdx.graphics.getHeight() - 20);

                { // Inventory
                    if (player.state == EnumPlayerState.IN_INVENTORY) {
                        layout.setText(bitmapFont, inventoryTextBuffer);
                        bitmapFont.draw(HUDBatch, layout, Gdx.graphics.getWidth() - layout.width - 20, Gdx.graphics.getHeight() - 10);
                    }
                    bitmapFont.draw(HUDBatch, infoTextBuffer, 10, 40);
                    bitmapFont.draw(HUDBatch, playerStatsTextBuffer, 10, Gdx.graphics.getHeight() - 10);
                }

                gameUI.draw(HUDBatch, 0);

                HUDBatch.end();
            }
        }
	}
	
	@Override
	public void dispose() {
		batch.dispose();
		bitmapFont.dispose();
	}

    private float timeSeconds = 0f;
    private float period = 1f;
	public void step() {
        //player.step();
        player.hasPlayed = false;

        timeSeconds +=Gdx.graphics.getRawDeltaTime();
        if(timeSeconds > period){
            timeSeconds-=period;
            player.hasPlayed = true;
        }


        if (player.hasPlayed) {
            for (Entity entity : currentFloor.entities) {
                if (entity.alive) {
                    entity.step();
                }
            }
        }
    }

    private boolean playerCanReachExit() {
        final GraphPath<Node> outPath = new DefaultGraphPath<Node>();
        outPath.clear();
        Tuple<Node, Node> pathNodes = player.getThisNodeAndExitNode(currentFloor);
        return currentFloor.pathfinder.searchNodePath(pathNodes.firstElement, pathNodes.secondElement, new ManhattanDistance(), outPath);
    }
}
