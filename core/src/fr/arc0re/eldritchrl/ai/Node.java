package fr.arc0re.eldritchrl.ai;

import com.badlogic.gdx.ai.pfa.Connection;
import com.badlogic.gdx.utils.Array;

public class Node {
    private final int index;
    private final int x;
    private final int y;
    private int value;
    private final Array<Connection<Node>> connections;

    public Node(int index, int x, int y, int value, final int capacity) {
        this.index = index;
        this.x = x;
        this.y = y;
        this.value = value;
        this.connections = new Array<Connection<Node>>(capacity);
    }

    public int getIndex() {
        return index;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Array<Connection<Node>> getConnections() {
        return connections;
    }
}
