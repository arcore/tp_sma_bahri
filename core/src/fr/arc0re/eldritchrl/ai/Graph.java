package fr.arc0re.eldritchrl.ai;

import com.badlogic.gdx.ai.pfa.Connection;
import com.badlogic.gdx.ai.pfa.indexed.IndexedGraph;
import com.badlogic.gdx.utils.Array;

public class Graph implements IndexedGraph<Node> {
    // THANKS BOSS https://www.programcreek.com/java-api-examples/?code=Mignet/Inspiration/Inspiration-master/core/src/com/v5ent/game/pfa/GraphGenerator.java#
    private Array<Node> nodes;

    public Graph(Array<Node> nodes) {
        this.setNodes(nodes);
    }

    @Override
    public int getIndex(Node node) {
        return node.getIndex();
    }

    @Override
    public int getNodeCount() {
        return getNodes().size;
    }

    @Override
    public Array<Connection<Node>> getConnections(Node fromNode) {
        return fromNode.getConnections();
    }

    public Array<Node> getNodes() {
        return nodes;
    }

    public void setNodes(Array<Node> nodes) {
        this.nodes = nodes;
    }

    public Node get(int index) {
        return nodes.get(index);
    }

    public void truncatePath(int index) {
        nodes.removeIndex(index);
    }

    public Node getNodeFromCoords(int x, int y) {
        for (Node node : nodes) {
            if (node.getX() == x && node.getY() == y) {
               return node;
            }
        }
        return null;
    }
}
