package fr.arc0re.eldritchrl.ai;

import com.badlogic.gdx.ai.pfa.DefaultConnection;
import com.badlogic.gdx.utils.Array;

public class GraphGenerator {
    public static Graph generateGraph(int[][] map, int numRows, int numCols) {
        final Node[][] nodes = new Node[numCols][numRows];
        final Array<Node> indexedNodes = new Array<Node>(numCols * numRows);

        int index = 0;
        for (int y = 0; y < numCols; y++) {
            for (int x = 0; x < numRows; x++, index++) {
                nodes[x][y] = new Node(index, x, y, map[x][y], 4);
                indexedNodes.add(nodes[x][y]);
            }
        }

        for (int y = 0; y < numRows; y++, index++) {
            for (int x = 0; x < numCols; x++, index++) {
                if (map[x][y] == 1) {
                    continue;
                }

                if (x - 1 >= 0 && map[x - 1][y] == 0) {
                    nodes[x][y].getConnections().add(new DefaultConnection<Node>(nodes[x][y], nodes[x - 1][y]));
                }

                if (x + 1 < numCols && map[x + 1][y] == 0) {
                    nodes[x][y].getConnections().add(new DefaultConnection<Node>(nodes[x][y], nodes[x + 1][y]));
                }

                if (y - 1 >= 0 && map[x][y - 1] == 0) {
                    nodes[x][y].getConnections().add(new DefaultConnection<Node>(nodes[x][y], nodes[x][y - 1]));
                }

                if (y + 1 < numRows && map[x][y + 1] == 0) {
                    nodes[x][y].getConnections().add(new DefaultConnection<Node>(nodes[x][y], nodes[x][y + 1]));
                }

            }
        }

        return new Graph(indexedNodes);
    }
}
