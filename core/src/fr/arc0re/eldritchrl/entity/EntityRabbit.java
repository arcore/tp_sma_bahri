package fr.arc0re.eldritchrl.entity;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import fr.arc0re.eldritchrl.floor.Floor;

import java.awt.*;

public class EntityRabbit extends EntityMonster {
    public EntityRabbit(Floor theFloor) {
        super(theFloor);
        textureAtlas = new TextureAtlas(Gdx.files.internal(Floor.TILES_TEXTURE_ATLAS_PACK_FILE));
        animationFrames = new Animation<TextureRegion>(0.033f, textureAtlas.findRegions("player_anim"), Animation.PlayMode.LOOP);
        screenCoords = new Point(0, 0);
        name = "Rat";
        nodesToAttackPlayer = 10;
        maxHealth = 10;
        health = maxHealth;
        attack = 5;
    }

    @Override
    protected void loadSprite() {
        sprite = new Sprite(textureAtlas.findRegion("rat"));
    }
}
