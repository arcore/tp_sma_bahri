package fr.arc0re.eldritchrl.entity;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.scenes.scene2d.Actor;
import fr.arc0re.eldritchrl.EldritchRL;
import fr.arc0re.eldritchrl.Tile;
import fr.arc0re.eldritchrl.ai.Node;
import fr.arc0re.eldritchrl.floor.Floor;
import fr.arc0re.eldritchrl.util.Tuple;

import java.awt.*;

public abstract class Entity extends Actor {
    public int health = 20;
    public int maxHealth = 20;
    public int sanity = 100;
    public int attack = 5;
    public boolean confused = false;
    public float speed = 0.5f;
    public boolean alive = true;
    public TextureAtlas textureAtlas;
    public Animation<TextureRegion> animationFrames;
    public Point screenCoords;
    public Floor currentFloor;
    private float stateTime;
    public String name;
    public String description;
    protected boolean useAnimationFrames = false;
    protected Sprite sprite;
    protected String spriteTextureName;
    public static final String DEFAULT_SPRITE_NAME = "no_tile";

    public abstract void step();

    public Entity(Floor currentFloor) {
        this.currentFloor = currentFloor;
        this.spriteTextureName = DEFAULT_SPRITE_NAME;
        textureAtlas = new TextureAtlas(Floor.TILES_TEXTURE_ATLAS_PACK_FILE);

        if (!useAnimationFrames) {
            loadSprite();
            sprite.flip(false, true);
        }
    }

    public void setCurrentFloor(Floor currentFloor) {
        this.currentFloor = currentFloor;
    }

    /** Override this to load your own sprite. Called by the constructor */
    protected void loadSprite() {
        sprite = new Sprite(textureAtlas.findRegion(spriteTextureName));
    }

    public void attack(Entity targetEntity) {
        if (this.alive && targetEntity.alive) {
            EldritchRL.infoTextBuffer = String.format("%s attacked %s! %s lost %d health points!",
                    this.name,
                    ((this instanceof Player && ((Player)this).inventory.equippedWeapon!=null)
                            ? (targetEntity.name + " with " + ((Player)this).inventory.equippedWeapon.name)
                            : targetEntity.name),
                    targetEntity.name, this.attack);
            targetEntity.health -= this.attack;

            if (targetEntity.health <= 0) {
                EldritchRL.infoTextBuffer += String.format(" %s is dead!\n", targetEntity.name);
                targetEntity.alive = false;

                if (this instanceof Player) {
                    ((Player)this).justKilled();
                }
            }
        }
    }

    @Override
    public void draw(Batch batch, float alpha) {
        if (useAnimationFrames) {
            stateTime += Gdx.graphics.getDeltaTime(); // Accumulate elapsed animation time
            TextureRegion frame = animationFrames.getKeyFrame(stateTime, true);
            batch.draw(frame, screenCoords.x, screenCoords.y);
        } else {
            Point tileCoords = currentFloor.getTileAtGameCoords(screenCoords.x, screenCoords.y).coords;
            sprite.setPosition(tileCoords.x * Floor.TILE_WIDTH, tileCoords.y * Floor.TILE_HEIGHT);
            //sprite.flip(false, true);
            sprite.draw(batch);
        }
    }

    /** Returns *this* node and the player node. */
    public Tuple<Node, Node> getStartNodeAndPlayerNode() {
        Point currentTileCoords = currentFloor.getTileAtGameCoords(screenCoords.x, screenCoords.y).coords;
        Point playerTileCoords = currentFloor.getPlayerRef().getCurrentTile().coords;

        Node startNode = currentFloor.graph.getNodeFromCoords(currentTileCoords.x, currentTileCoords.y);
        Node endNode = currentFloor.graph.getNodeFromCoords(playerTileCoords.x, playerTileCoords.y);

        return new Tuple<Node, Node>(startNode, endNode);
    }

    public Tile getCurrentTile() {
        return currentFloor.getTileAtGameCoords(screenCoords.x, screenCoords.y);
    }

    public void move(int x, int y) {
        // Remove self from old tile
        getCurrentTile().removeEntity();

        // Set new entity pos
        screenCoords.x = x;
        screenCoords.y = y;

        // Set new tile
        getCurrentTile().addEntity(this);
    }

    public Sprite getSprite() {
        return sprite;
    }

    public void setSprite(Sprite sprite) {
        this.sprite = sprite;
    }
}
