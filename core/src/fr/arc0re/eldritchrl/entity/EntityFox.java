package fr.arc0re.eldritchrl.entity;

import com.badlogic.gdx.graphics.g2d.Sprite;
import fr.arc0re.eldritchrl.floor.Floor;

import java.awt.*;

public class EntityFox extends EntityMonster {
    public EntityFox(Floor currentFloor) {
        super(currentFloor);
        screenCoords = new Point(0, 0);
        name = "Serpent";
        nodesToAttackPlayer = 20;
        maxHealth = 15;
        health = maxHealth;
        attack = 6;
    }

    @Override
    protected void loadSprite() {
        sprite = new Sprite(textureAtlas.findRegion("serpent"));
    }
}
