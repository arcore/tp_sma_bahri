package fr.arc0re.eldritchrl.entity;

public enum EnumPlayerState {
    IN_GAME,
    IN_INVENTORY,
    IN_INVENTORY_POPUP,
    FIGHTING,
    DEAD
}
