package fr.arc0re.eldritchrl.entity;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.*;
import fr.arc0re.eldritchrl.Defines;
import fr.arc0re.eldritchrl.EldritchRL;
import fr.arc0re.eldritchrl.Inventory;
import fr.arc0re.eldritchrl.Tile;
import fr.arc0re.eldritchrl.ai.Node;
import fr.arc0re.eldritchrl.floor.Floor;
import fr.arc0re.eldritchrl.item.Item;
import fr.arc0re.eldritchrl.item.ItemWeapon;
import fr.arc0re.eldritchrl.util.Tuple;

import java.awt.*;

@SuppressWarnings("FieldCanBeLocal")

public class Player extends Entity {
    public static final int STARTING_HEALTH = 100;
    public static final int STARTING_STRENGH = 10;
    public Inventory inventory;
    private final float frameDuration = 0.033f;
    private final String animationName = "player_anim";
    private float stateTime = .0f;
    private int stepX = Floor.TILE_WIDTH;
    private int stepY = Floor.TILE_HEIGHT;
    private int mvtX = 0;
    private int mvtY = 0;
    public boolean playerHasMoved = false;
    public boolean playerIsMoving = false;
    private String debugStr;
    public EnumPlayerState state;
    private static GlyphLayout glyphLayout = new GlyphLayout();
    private EldritchRL gameInstance;
    public boolean hasPlayed = false;
    public boolean godmode = false;
    private final int xpPerKill = 10;
    public int level = 1;
    public final int startingXpBeforeLvlUp = 100;
    public int xpBeforeLvlUp = startingXpBeforeLvlUp;
    private final int healthReward = 10;
    private final int attackReward = 10;

    public Player(Floor theFloor) {
        super(theFloor);

        health = STARTING_HEALTH;
        attack = STARTING_STRENGH;
        maxHealth = STARTING_HEALTH;

        textureAtlas = new TextureAtlas(Gdx.files.internal(Floor.TILES_TEXTURE_ATLAS_PACK_FILE));
        animationFrames = new Animation<TextureRegion>(frameDuration, textureAtlas.findRegions(animationName), Animation.PlayMode.LOOP);
        screenCoords = new Point(0, 0);
        inventory = new Inventory();
        state = EnumPlayerState.IN_GAME;
        name = "The wanderer";

        godmode = false;
        if (godmode) {
            maxHealth = 999;
        }
    }

    @Override
    protected void loadSprite() {
        sprite = new Sprite(textureAtlas.findRegion("player_ascii"));
    }

    public Tile[] getEightAdjacentTiles() {
        Tile[] adjacentTiles = new Tile[8];
        Tile playerTile = getCurrentTile();
        adjacentTiles[0] = currentFloor.getTileAt(playerTile.coords.x, playerTile.coords.y-1); // UP
        adjacentTiles[1] = currentFloor.getTileAt(playerTile.coords.x, playerTile.coords.y+1); // DOWN
        adjacentTiles[2] = currentFloor.getTileAt(playerTile.coords.x-1, playerTile.coords.y); // LEFT
        adjacentTiles[3] = currentFloor.getTileAt(playerTile.coords.x+1, playerTile.coords.y); // RIGHT
        adjacentTiles[4] = currentFloor.getTileAt(playerTile.coords.x-1, playerTile.coords.y-1); // TOP-LEFT
        adjacentTiles[5] = currentFloor.getTileAt(playerTile.coords.x+1, playerTile.coords.y-1); // TOP-RIGHT
        adjacentTiles[6] = currentFloor.getTileAt(playerTile.coords.x-1, playerTile.coords.y+1); // BOTTOM-LEFT
        adjacentTiles[7] = currentFloor.getTileAt(playerTile.coords.x+1, playerTile.coords.y+1); // BOTTOM-RIGHT
        return adjacentTiles;
    }

    public EldritchRL getGameInstance() {
        return gameInstance;
    }

    public void setGameInstance(EldritchRL gameInstance) {
        this.gameInstance = gameInstance;
    }

    //TODO: check why this is rendering upside down
    @Override
    public void draw(Batch batch, float alpha) {
        /*stateTime += Gdx.graphics.getDeltaTime(); // Accumulate elapsed animation time
        TextureRegion frame = animationFrames.getKeyFrame(stateTime, true);
        batch.draw(frame, screenCoords.x, screenCoords.y);*/
        super.draw(batch, 0);

        if (state == EnumPlayerState.IN_INVENTORY) {
            drawInventory();
        }
    }

    public void drawInventory() {
        if (inventory != null) {
            EldritchRL.inventoryTextBuffer = String.format("%s\n", EldritchRL.gameStrings.getTraductionFor(Defines.Strings.INVENTORY_INVENTORY));
            for (int i = 0; i < inventory.items.size(); i++) {
                Item item = inventory.items.get(i);
                if (item != null) {
                    String selectedMark = " ";
                    if (inventory.selectedItemIndex == i) selectedMark = ">";
                    EldritchRL.inventoryTextBuffer += String.format("  %s %s\n", selectedMark, item.name);
                }
            }
        }
    }

    @Override
    public void step() {
        playerHasMoved = false;
        playerIsMoving = false;
        mvtX = mvtY = 0;
        hasPlayed = false;
        Tile currentTile = getCurrentTile();

        if (godmode) health = maxHealth;

        if (health <= 0 || !alive) {
            state = EnumPlayerState.DEAD;
            return;
        }

        calculatePlayerFOV();

        switch (state) {
            case IN_GAME: {

                // Player movement
                // TODO: use state machine?
//                if (isKeyJustPressed(Defines.PlayerBindings.LEFT) && !playerHasMoved) {
//                    mvtX -= stepX;
//                    playerIsMoving = true;
//                    System.out.println("Player wants to move LEFT");
//                } else if (isKeyJustPressed(Defines.PlayerBindings.RIGHT) && !playerHasMoved) {
//                    mvtX += stepX;
//                    playerIsMoving = true;
//                    System.out.println("Player wants to move RIGHT");
//                } else if (isKeyJustPressed(Defines.PlayerBindings.DOWN) && !playerHasMoved) {
//                    mvtY += stepY;
//                    playerIsMoving = true;
//                    System.out.println("Player wants to move DOWN");
//                } else if (isKeyJustPressed(Defines.PlayerBindings.UP) && !playerHasMoved) {
//                    mvtY -= stepY;
//                    playerIsMoving = true;
//                    System.out.println("Player wants to move UP");
//                } else if (isKeyJustPressed(Defines.PlayerBindings.INV)) {
//                    state = EnumPlayerState.IN_INVENTORY;
//                }
            } break;

            case IN_INVENTORY: {

                // Menu selection management
                if (isKeyJustPressed(Input.Keys.DOWN)) {
                    if (inventory.selectedItemIndex < inventory.items.size() - 1) {
                        if (inventory.selectedItemIndex++ > inventory.items.size()) inventory.updateMenu(inventory.selectedItemIndex);
                        else inventory.updateMenu(inventory.selectedItemIndex++);
                    }
                } else if (isKeyJustPressed(Input.Keys.UP)) {
                    if (inventory.selectedItemIndex <= inventory.items.size() && inventory.selectedItemIndex > 0) {
                        if (inventory.selectedItemIndex-- < 0) inventory.updateMenu(inventory.selectedItemIndex);
                        else inventory.updateMenu(inventory.selectedItemIndex--);
                    }
                }

                // Show item description, stats, etc...
                if (isKeyJustPressed(Input.Keys.ENTER) && inventory.items.size() > 0) {
                    try {
                        Item item = inventory.items.get(inventory.selectedItemIndex);
                        System.out.println(item.description);
                        if (item instanceof ItemWeapon) gameInstance.getGameUI().eStr = "Equip";
                        else gameInstance.getGameUI().eStr = "Use";
                        gameInstance.getGameUI().showPopup(item.description);

                        state = EnumPlayerState.IN_INVENTORY_POPUP;
                    } catch (IndexOutOfBoundsException ex) {
                        ex.printStackTrace();
                        state = EnumPlayerState.IN_GAME;
                    }
                }

                // Closing inventory, cleaning up
                if (isKeyJustPressed(Input.Keys.ESCAPE) || isKeyJustPressed(Defines.PlayerBindings.INV)) {
                    state = EnumPlayerState.IN_GAME;
                    inventory.resetMenu(); // Set selections
                }
            } break;

            case IN_INVENTORY_POPUP: {
                Item item = inventory.items.get(inventory.selectedItemIndex);

                if (isKeyJustPressed(Input.Keys.E)) { // Use item
                    item.use(this);
                    inventory.items.remove(item);

                    gameInstance.getGameUI().killPopup();

                    // If the inventory is empty after we used our object, no need to keep it open.
                    if (inventory.items.size() == 0) {
                        state = EnumPlayerState.IN_GAME;
                    } else {
                        state = EnumPlayerState.IN_INVENTORY;
                    }

                    hasPlayed = true;

                } else if (isKeyJustPressed(Input.Keys.C)) { // Cancel
                    gameInstance.getGameUI().killPopup();
                    state = EnumPlayerState.IN_INVENTORY;

                } else if (isKeyJustPressed(Input.Keys.D)) { // Discard
                    Tile playerTile = getCurrentTile();
                    Tile[] adjacentTiles = new Tile[4];
                    boolean droppedItem = false;

                    gameInstance.getGameUI().killPopup();
                    inventory.items.remove(item);

                    // Sorted by "priority" on where to drop the item:
                    adjacentTiles[0] = currentFloor.getTileAt(playerTile.coords.x, playerTile.coords.y+1); // DOWN
                    adjacentTiles[1] = currentFloor.getTileAt(playerTile.coords.x-1, playerTile.coords.y); // LEFT
                    adjacentTiles[2] = currentFloor.getTileAt(playerTile.coords.x+1, playerTile.coords.y); // RIGHT
                    adjacentTiles[3] = currentFloor.getTileAt(playerTile.coords.x, playerTile.coords.y-1); // UP

                    for (Tile tile : adjacentTiles) {
                        if (tile.isWalkable && !tile.containsEntity) {
                            // We found a tile where to drop our item
                            tile.items.push(item);
                            droppedItem = true;
                            break;
                        }
                    }

                    if (!droppedItem) {
                        EldritchRL.infoTextBuffer = "Failed to drop item " + item.name + "! The player is surrounded!";
                    } else {
                        EldritchRL.infoTextBuffer = "Dropped item " + item.name;
                    }

                    // If the inventory is empty after we discarded our object, no need to keep it open.
                    if (inventory.items.size() == 0) {
                        state = EnumPlayerState.IN_GAME;
                    } else {
                        state = EnumPlayerState.IN_INVENTORY;
                    }
                }

            } break;
        }

        if (playerIsMoving) {
            move(mvtX, mvtY);
            if (!playerHasMoved) playerIsMoving = false;
            if (playerHasMoved) {
                Tile newPlayerTile = getCurrentTile();
                newPlayerTile.containsEntity = true;
                newPlayerTile.addEntity(this);
                currentTile.containsEntity = false; // currentTile is actually the former tile here
                currentTile.removeEntity();

                hasPlayed = true;
            }
        }

        // Item pickup
        if (currentTile != null) {
            if (currentTile.items.size() > 0) {
                while (currentTile.items.size() > 0) {
                    //System.out.printf("Player picked up item: %s!\n", currentTile.peekTopItem().name);
                    EldritchRL.infoTextBuffer = String.format("You picked up a %s!\n", currentTile.peekTopItem().name.toLowerCase());
                    inventory.items.add(currentTile.getTopItem());
                    inventory.resetMenu();
                }
            }
        }
    }

    private boolean isKeyJustPressed(int key) {
        return Gdx.input.isKeyJustPressed(key);
    }

    private void calculatePlayerFOV() {
        // Taken from http://www.roguebasin.com/index.php?title=Eligloscode
        final double magicNumber = 0.01745f; // Number of radiants in a degree
        double x, y;
        int i;

        for (i = 0; i < 360; i++) {
            x = Math.cos((double)i*magicNumber);
            y = Math.sin((double)i*magicNumber);
            doFov(x, y);
        }
    }

    private void doFov(double x, double y) {
        // Taken from http://www.roguebasin.com/index.php?title=Eligloscode
        int i;
        double ox, oy;
        Tile playerTile = getCurrentTile();
        final int VIEW_RADIUS = 5;//10;
        ox = (double)playerTile.coords.x+0.5;
        oy = (double)playerTile.coords.y+0.5;
        for (i = 0; i < VIEW_RADIUS; i++) {
            Tile tile = currentFloor.getTileAt((int)ox, (int)oy);
            if (tile == null)
                return;
            tile.discovered = true;
            if (!tile.isWalkable)
                return;
            ox += x;
            oy += y;
        }
    }

    @Override
    public void move(int x, int y) {
        if (playerIsMoving && !playerHasMoved) {
            if (canMove(x, y)) {
                screenCoords.x += x;
                screenCoords.y += y;
                playerHasMoved = true;
                playerIsMoving = false;
            } else {
                Tile tile = currentFloor.getTileAtGameCoords(screenCoords.x + x, screenCoords.y + y);
                if (tile != null) System.out.printf("Player cannot move there -> (%d, %d) [%d, %d]\n", screenCoords.x + x, screenCoords.y + y, tile.coords.x, tile.coords.y);
                else System.out.printf("Player cannot move there -> (%d, %d) [NULL TILE]\n", x, y);
            }
        }
    }

    /** Tells if the player can move to the new coords (movX and movY) */
    private boolean canMove(int x, int y) {
        Tile playerTile = getCurrentTile();
        if (playerTile == null) return false;
        if (playerTile.isFloorExit) return false;

        Tile[] adjacentTiles = new Tile[4];
        adjacentTiles[0] = currentFloor.getTileAt(playerTile.coords.x, playerTile.coords.y-1); // UP   //TODO: fixme
        adjacentTiles[1] = currentFloor.getTileAt(playerTile.coords.x, playerTile.coords.y+1); // DOWN
        adjacentTiles[2] = currentFloor.getTileAt(playerTile.coords.x-1, playerTile.coords.y); // LEFT
        adjacentTiles[3] = currentFloor.getTileAt(playerTile.coords.x+1, playerTile.coords.y); // RIGHT

        int playerX = currentFloor.getScreenCoordsOfTile(playerTile).x;
        int playerY = currentFloor.getScreenCoordsOfTile(playerTile).y;
        int newPlayerX = playerX + x;
        int newPlayerY = playerY + y;

        if (playerY != newPlayerY) {
            if (adjacentTiles[0] != null && newPlayerY < playerY && adjacentTiles[0].isWalkable) return moveOrAttack(adjacentTiles[0]);
            if (adjacentTiles[1] != null && newPlayerY > playerY && adjacentTiles[1].isWalkable) return moveOrAttack(adjacentTiles[1]);
        }

        if (playerX != newPlayerX) {
            if (adjacentTiles[2] != null && newPlayerX < playerX && adjacentTiles[2].isWalkable) return moveOrAttack(adjacentTiles[2]);
            if (adjacentTiles[3] != null && newPlayerX > playerX && adjacentTiles[3].isWalkable) return moveOrAttack(adjacentTiles[3]);
        }

        return false;
    }

    private boolean moveOrAttack(Tile destinationTile) {
        if (!destinationTile.containsEntity) {
            return true;
        } else { // Its an enemy gunship!
            attack(destinationTile.getCurrentEntity());
            return false;
        }
    }

    public void resetPlayerStrength() {
        attack = STARTING_STRENGH;
    }

    public void justKilled() {
        xpBeforeLvlUp -= xpPerKill;
        if (xpBeforeLvlUp <= 0) levelUp();
    }

    public void levelUp() {
        int prevLevel = this.level;
        this.level++;
        xpBeforeLvlUp = startingXpBeforeLvlUp+(prevLevel*10);
        this.maxHealth += healthReward;
        this.attack += attackReward;
        this.health = maxHealth;
        EldritchRL.infoTextBuffer = String.format("You reached level %d! You feel better.\n", level);
    }

    /** Returns the player node and the exit tile node. */
    public Tuple<Node, Node> getThisNodeAndExitNode(Floor floor) {
        Point playerTileCoords = floor.getPlayerRef().getCurrentTile().coords;
        Point exitTileCoords = floor.exitTile.coords;

        Node startNode = floor.graph.getNodeFromCoords(playerTileCoords.x, playerTileCoords.y);
        Node endNode = floor.graph.getNodeFromCoords(exitTileCoords.x, exitTileCoords.y);

        return new Tuple<Node, Node>(startNode, endNode);
    }
}
