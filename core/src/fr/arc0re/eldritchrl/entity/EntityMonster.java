package fr.arc0re.eldritchrl.entity;

import com.badlogic.gdx.ai.pfa.DefaultGraphPath;
import com.badlogic.gdx.ai.pfa.GraphPath;
import fr.arc0re.eldritchrl.Tile;
import fr.arc0re.eldritchrl.ai.ManhattanDistance;
import fr.arc0re.eldritchrl.ai.Node;
import fr.arc0re.eldritchrl.floor.Floor;
import fr.arc0re.eldritchrl.util.Tuple;

import java.awt.*;

public class EntityMonster extends Entity {
    protected final int turnsBeforeRecalculatingPlayerPos = 10;
    protected int turnsDone = 0;
    protected final GraphPath<Node> outPath = new DefaultGraphPath<Node>();
    protected boolean successfullyRecalculatedPath = false;
    /** The monster won't try to reach the player before it only takes *num*. */
    protected int nodesToAttackPlayer = 20;

    public EntityMonster(Floor currentFloor) {
        super(currentFloor);
    }

    @Override
    public final void step() {
        if (health <= 0) { // TODO: Doesn't work? check why
            alive = false;
            return;
        }

        recalculatePathToPlayer();

        if (successfullyRecalculatedPath) {
            // We skip the first one cause its the current position
            //if (outPath.getCount() > nodesToAttackPlayer) return; // We don't want all the enemies to go hunt the enemy as soon as the game begins.
            //System.out.println("nodes for rat: " + outPath.getCount());
            for (int i = 1; i < outPath.getCount(); i++) {
                int x = outPath.get(i).getX();
                int y = outPath.get(i).getY();
                Point nextNode = currentFloor.getScreenCoordsOfTile(x, y);
                if (nextNode != null) {
                    Tile nextTile = currentFloor.getTileAt(x, y);
                    if (nextTile.containsEntity && nextTile.getCurrentEntity() instanceof Player) {
                        attack(nextTile.getCurrentEntity());
                    } else {
                        move(nextNode.x, nextNode.y);
                    }
                    return;
                } else {
                    System.out.println("%%%%%%%%%%%%%% nextNode is null wtf");
                }
            }
        }

        turnsDone++;
    }

    protected void recalculatePathToPlayer() {
        outPath.clear();
        Tuple<Node, Node> pathNodes = getStartNodeAndPlayerNode();
        successfullyRecalculatedPath = currentFloor.pathfinder.searchNodePath(pathNodes.firstElement, pathNodes.secondElement, new ManhattanDistance(), outPath);
    }
}
