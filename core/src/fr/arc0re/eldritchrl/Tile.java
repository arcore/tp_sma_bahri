package fr.arc0re.eldritchrl;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import fr.arc0re.eldritchrl.entity.Entity;
import fr.arc0re.eldritchrl.floor.Floor;
import fr.arc0re.eldritchrl.item.Item;

import java.awt.*;
import java.util.ArrayDeque;
import java.util.Deque;

public class Tile {
    public boolean isWalkable;
    public boolean hasGas;
    public String name;
    public Point coords;
    public int floorIntValue;
    public TextureAtlas.AtlasRegion textureRegion;
    public Deque<Item> items;
    private Entity currentEntity;
    public boolean discovered, containsEntity, isFloorExit;

    public Tile(boolean isWalkable, boolean hasGas, String name, Point coords, int floorIntValue, TextureAtlas.AtlasRegion textureRegion) {
        this.isWalkable = isWalkable;
        this.hasGas = hasGas;
        this.name = name;
        this.coords = coords;
        this.floorIntValue = floorIntValue;
        this.textureRegion = textureRegion;
        discovered = false;
        items = new ArrayDeque<Item>();
    }

    public Tile(boolean isWalkable, String name, Point coords, int floorIntValue) {
        this.isWalkable = isWalkable;
        this.hasGas = false;
        this.name = name;
        this.coords = coords;
        this.floorIntValue = floorIntValue;
        this.textureRegion = Floor.tilesTexture.findRegion(name);
        discovered = false;
        items = new ArrayDeque<Item>();
    }

    public Tile(Tile tileToCopy) {
        this.isWalkable = tileToCopy.isWalkable;
        this.hasGas = tileToCopy.hasGas;
        this.name = tileToCopy.name;
        this.coords = tileToCopy.coords;
        this.floorIntValue = tileToCopy.floorIntValue;
        this.textureRegion = tileToCopy.textureRegion;
        this.discovered = tileToCopy.discovered;
        this.items = new ArrayDeque<Item>(tileToCopy.items);
    }

    public String toString() {
        return String.format("isWalkable: %b\nname: %s\ncoords: (%d,%d)\n", isWalkable, name, coords.x, coords.y);
    }

    public void addItem(Item item) {
        if (items != null && item != null) {
            items.push(item);
        }
    }

    /** Pops the first item (last inserted) of the items stack. */
    public Item getTopItem() {
        return items.pop();
    }

    /** Gets the first item (last inserted) of the items stack */
    public Item peekTopItem() {
        return items.peek();
    }

    public Entity getCurrentEntity() {
        return currentEntity;
    }

    public void addEntity(Entity currentEntity) {
        this.currentEntity = currentEntity;
        containsEntity = true;
    }

    public void removeEntity() {
        currentEntity = null;
        containsEntity = false;
    }

    public void setAsExit() {
        isFloorExit = true;
    }
}
