package fr.arc0re.eldritchrl;

import fr.arc0re.eldritchrl.item.Item;
import fr.arc0re.eldritchrl.item.ItemWeapon;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Inventory {
    public List<Item> items; // TODO: STACK SAME ITEMS
    public ItemWeapon equippedWeapon;
    /** Item, isSelected */
    public Map<Integer, Boolean> menuEntries;
    public Integer selectedItemIndex;

    public Inventory() {
        items = new ArrayList<Item>();
        menuEntries = new HashMap<Integer, Boolean>();
    }

    public void resetMenu() {
        if (items != null && items.size() > 0) {
            for (int i = 0; i < items.size(); i++) {
                menuEntries.put(i, false);
            }
            menuEntries.put(0, true); // First item selected.
            this.selectedItemIndex = 0;
        }
    }

    public void updateMenu(Integer selectedItem) {
        for (int i = 0; i < items.size(); i++) {
            menuEntries.put(i, false);
        }
        menuEntries.put(selectedItem, true);
        this.selectedItemIndex = selectedItem;
    }
}
