package fr.arc0re.eldritchrl.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import fr.arc0re.eldritchrl.Defines;
import fr.arc0re.eldritchrl.EldritchRL;

public class DesktopLauncher {
	public static void main(String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		config.width = Defines.DESKTOP_WINDOW_WIDTH;
		config.height = Defines.DESKTOP_WINDOW_HEIGHT;
		config.title = Defines.DESKTOP_WINDOW_TITLE;

		new LwjglApplication(new EldritchRL(), config);
	}
}
